<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use app\models\Status;
use app\models\Level;
/**
 * This is the model class for table "breakdown".
 *
 * @property integer $id
 * @property string $title
 * @property integer $levelId
 * @property integer $statusId
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['levelId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'levelId' => 'Level ID',
            'statusId' => 'Status ID',
        ];
    }
	
	
	public function beforeSave($insert)
    {
	
		if(parent::beforeSave($insert))
		{
			if ($this->isNewRecord)
			{
				$model = new Breakdown;
				$this->statusId = 1;
		}
	}	
		return parent::beforeSave($insert);
	}
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
    }
	
public function getLevelItem()
    {
        return $this->hasOne(Level::className(), ['id' => 'levelId']);
    }
}
