<?php

use yii\db\Migration;

/**
 * Handles the creation of table `level`.
 */
class m170718_060122_create_level_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('level', [
            'id' => 'pk',
			'name' => 'string',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('level');
    }
}
