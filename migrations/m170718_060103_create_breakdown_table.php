<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m170718_060103_create_breakdown_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('breakdown', [
            'id' => 'pk',
			'title' => 'string',
			'levelId' => 'integer',
			'statusId' => 'integer',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('breakdown');
    }
}
