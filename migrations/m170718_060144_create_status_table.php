<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170718_060144_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => 'pk',
			'name' => 'string',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
